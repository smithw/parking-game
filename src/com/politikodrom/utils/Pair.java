package com.politikodrom.utils;

public class Pair<A, B> {
	// Based on this answer on StackOverflow: http://stackoverflow.com/a/677248
	private final A first;
	private final B second;

	public Pair(A first, B second) {
		this.first = first;
		this.second = second;
	}

	public int hashCode() {
		int first, second;

		first = (this.first == null) ? 0 : this.first.hashCode();
		second = (this.second == null) ? 0 : this.second.hashCode();

		return (first + second) * second + first;
	}

	public boolean equals(Object obj) {
		boolean equal, firstEqual, secondEqual;
		Pair<?, ?> other;

		equal = false;

		if (obj instanceof Pair) {
			other = (Pair<?, ?>) obj;

			firstEqual = ((this.first == null) ^ (other.first == null)) ?
				false :
				((this.first == null) || this.first.equals(other.first));

			secondEqual = ((this.second == null) ^ (other.second == null)) ?
				false :
				((this.second == null) || this.second.equals(other.second));

			equal = firstEqual && secondEqual;
		}

		return equal;
	}

	public A getFirst() {
		return this.first;
	}

	public B getSecond() {
		return this.second;
	}

	public String toString() {
		return "(" + this.first + "," + this.second + ")";
	}
}