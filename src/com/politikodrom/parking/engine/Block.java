package com.politikodrom.parking.engine;

import com.politikodrom.utils.Pair;

public class Block extends Pair<Integer, Integer> {
	public Block(int x, int y) {
		super(new Integer(x), new Integer(y));
	}

	public Block() {
		super(new Integer(0), new Integer(0));
	}

	public Block(Integer x, Integer y) {
		super(x, y);
	}

	public int getX() {
		return this.getFirst().intValue();
	}

	public int getY() {
		return this.getSecond().intValue();
	}
}