package com.politikodrom.parking.engine;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import java.util.List;
import java.util.ArrayList;

@Root(name="element")
public class Vehicle {
	@Attribute(name="player")
	private boolean isPlayer;

	@Attribute
	private int x;

	@Attribute
	private int y;

	@Attribute(name="w")
	private int width;

	@Attribute(name="h")
	private int height;

	public Vehicle() {
		this.setX(0);
		this.setY(0);
		this.setIsPlayer(false);
		this.setWidth(1);
		this.setHeight(1);
	}

	public Movement movementType() {
		// Nullable
		Movement m;
		boolean wIsOne, hIsOne;

		wIsOne = this.getWidth() == 1;
		hIsOne = this.getHeight() == 1;

		if (wIsOne ^ hIsOne) {
			if (wIsOne) m = Movement.VERTICAL;
			else m = Movement.HORIZONTAL;
		}
		else m = null;

		return m;
	}

	public void setIsPlayer(@Attribute(name="player") boolean newIsPlayer) {
		this.isPlayer = newIsPlayer;
	}
	
	public boolean getIsPlayer() {
		return this.isPlayer;
	}

	public void setX(@Attribute(name="x") int newX) {
		this.x = newX;
	}
	
	public int getX() {
		return this.x;
	}

	public void setY(@Attribute(name="y") int newY) {
		this.y = newY;
	}
	
	public int getY() {
		return this.y;
	}

	public void setWidth(@Attribute(name="w") int newWidth) {
		this.width = newWidth;
	}
	
	public int getWidth() {
		return this.width;
	}

	public void setHeight(@Attribute(name="h") int newHeight) {
		this.height = newHeight;
	}
	
	public int getHeight() {
		return this.height;
	}

	public List<Block> getUsedBlocks() {
		List<Block> blocks;
		int i, j;

		blocks = new ArrayList<Block>();

		for (j = 0; j < this.height; j++) {
			for (i = 0; i < this.width; i++) {
				blocks.add(new Block(this.x + i, this.y + j));
			}
		}

		return blocks;
	}

	public Block topLeft() {
		return new Block(this.x, this.y);
	}

	public Block topRight() {
		return new Block(this.x + this.width - 1, this.y);
	}

	public Block bottomLeft() {
		return new Block(this.x, this.y + this.height - 1);
	}

	public Block bottomRight() {
		return new Block(this.x + this.width - 1, this.y + this.height - 1);
	}
}