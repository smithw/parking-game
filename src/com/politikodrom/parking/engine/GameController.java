package com.politikodrom.parking.engine;

import com.politikodrom.parking.engine.exceptions.*;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.io.IOException;
import java.io.StringWriter;
import java.io.File;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

public class GameController {
	private ParkingUI parkingUI;
	private Grid grid;
	private List<Level> levels;
	private ListIterator<Level> currentLevel;

	public GameController(ParkingUI parkingUI) throws IOException {
		List<Level> levels;
		String levelDescription;

		this.parkingUI = parkingUI;
		this.parkingUI.showMessage("Hello world!");

		levels = Level.getLevels();

		if (levels != null) {
			if (levels.size() == 0) throw new IOException("Levels folder is empty!");

			this.setLevels(levels);
			this.setCurrentLevel(levels.listIterator());
		}
		else throw new IOException("Can't find levels folder!");
	}

	public void loop() throws PlayerQuitException {
		Level current;
		boolean played;

		played = false;

		while (this.currentLevel.hasNext()) {
			current = this.currentLevel.next();

			if (!current.getIsBeaten()) {
				played = true;
				this.playLevel(current);
			}
		}

		if (!played) {
			this.parkingUI.showMessage("Do you want to reset all levels?");
		}
	}

	public void playLevel(Level l) throws PlayerQuitException {
		this.parkingUI.showMessage("Playing level: " + l.getTitle());

		try {
			this.setGrid(new Grid(l));

			this.parkingUI.showMessage("Map: " + this.getGrid().getBlockMap());
		}
		catch (InconsistentGridException e) {
			this.parkingUI.showError("Repeated block: " + e.getMessage());	
			this.parkingUI.abort();
		}
	}

	public void setGrid(Grid newGrid) {
		this.grid = newGrid;
	}
	
	public Grid getGrid() {
		return this.grid;
	}

	public void setLevels(List<Level> newLevels) {
		this.levels = newLevels;
	}
	
	public List<Level> getLevels() {
		return this.levels;
	}

	public void setCurrentLevel(ListIterator<Level> newCurrentLevel) {
		this.currentLevel = newCurrentLevel;
	}
	
	public ListIterator<Level> getCurrentLevel() {
		return this.currentLevel;
	}
}
