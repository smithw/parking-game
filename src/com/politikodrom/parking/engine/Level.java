package com.politikodrom.parking.engine;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.io.File;
import java.io.StringWriter;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

@Root(name="level")
public class Level {
	@Element
	private String title;

	@ElementList(name="elements")
	private List<Vehicle> vehicles;

	@Element(name="beaten")
	private boolean isBeaten;

	private boolean isRandom;

	private File origin;

	public static final String path = "levels/";

	public Level(@Element(name="title") String title, @ElementList(name="elements") List<Vehicle> vehicles, @Element(name="beaten") boolean isBeaten) {
		this.title = title;
		this.vehicles = vehicles;
		this.isRandom = false;
		this.isBeaten = isBeaten;
	}

	public Level() {
		this.randomize();
	}

	public void randomize() {
		
	}

	public String description() {
		// Nullable
		Serializer serializer = new Persister();
		StringWriter sw = new StringWriter();

		try {
			serializer.write(this, sw);
		}
		catch (Exception e) {
			return null;
		}

		return sw.getBuffer().toString();
	}

	public static Level read(String name) throws Exception {
		Serializer serializer = new Persister();
		File source = new File(Level.path + name);
		Level lvl = serializer.read(Level.class, source);

		lvl.setOrigin(source);

		return lvl;
	}

	public static Level read(File file) throws Exception {
		Serializer serializer = new Persister();
		Level lvl = serializer.read(Level.class, file);

		lvl.setOrigin(file);

		return lvl;
	}

	public boolean save() throws Exception {
		if (this.origin != null) {
			Serializer serializer = new Persister();

			serializer.write(this, this.origin);

			return true;
		}
		else return false;
	}

	public void saveAs(File f) throws Exception {
		Serializer serializer = new Persister();

		serializer.write(this, f);
	}

	public void saveAs(String s) throws Exception {
		Serializer serializer = new Persister();
		File f = new File(Level.path + s);

		serializer.write(this, f);
	}

	public static List<Level> getLevels() {
		// Nullable
		File root;
		List<File> files;
		List<Level> levels;

		root = new File(Level.path);

		try {
			files = Arrays.asList(root.listFiles());
		}
		catch (Exception e) {
			return null;
		}

		levels = new ArrayList<Level>();

		for (File cursor : files) {
			try {
				levels.add(Level.read(cursor));
			}
			catch (Exception e) {
				continue; // for clarity's sake
			}
		}

		return levels;

	}

	public void setIsRandom(boolean newIsRandom) {
		this.isRandom = newIsRandom;
	}
	
	public boolean getIsRandom() {
		return this.isRandom;
	}

	public void setIsBeaten(boolean newIsBeaten) {
		this.isBeaten = newIsBeaten;
	}
	
	public boolean getIsBeaten() {
		return this.isBeaten;
	}

	public String getTitle() {
		return this.title;
	}

	public List<Vehicle> getVehicles() {
		return this.vehicles;
	}

	public void setOrigin(File newOrigin) {
		this.origin = newOrigin;
	}
	
	public File getOrigin() {
		return this.origin;
	}
}
