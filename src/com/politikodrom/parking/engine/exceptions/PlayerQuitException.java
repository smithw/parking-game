package com.politikodrom.parking.engine.exceptions;

public class PlayerQuitException extends Exception {
	private static final long serialVersionUID = 1L;

	public PlayerQuitException() {
		super("Good luck next time!");
	}
}
