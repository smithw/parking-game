package com.politikodrom.parking.engine.exceptions;

public class InconsistentGridException extends Exception {
	private static final long serialVersionUID = 1L;

	public InconsistentGridException(String s) {
		super(s);
	}
}