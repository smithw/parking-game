package com.politikodrom.parking.engine;

import java.lang.Boolean;

public interface ParkingUI {
	void showMessage(String message);
	void showError(String message);
	void abort();
	boolean getBooleanInput(String message, Boolean def);
	void updateGrid(Grid g);
}