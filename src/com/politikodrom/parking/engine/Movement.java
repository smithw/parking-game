package com.politikodrom.parking.engine;

public enum Movement {
	VERTICAL, HORIZONTAL
}