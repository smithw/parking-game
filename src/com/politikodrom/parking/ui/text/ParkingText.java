package com.politikodrom.parking.ui.text;

import com.politikodrom.parking.engine.*;

import java.lang.Boolean;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class ParkingText implements ParkingUI {
	public static void main(String[] args) {
		ParkingText textUI = new ParkingText();

		try {
			GameController controller = new GameController(textUI);

			controller.loop();
		}
		catch (Exception e) {
			textUI.showError(e.getMessage());
		}
	}

	public void showMessage(String message) {
		System.out.println("[MSG] ".concat(message));
	}

	public void showError(String message) {
		System.out.println("[ERROR] " + message);
	}

	public void abort() {
		System.exit(1);
	}

	public boolean getBooleanInput(String message, Boolean def) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String answer, fullMessage;
		String[] acceptArray = { "y", "Y", "n", "N" };
		Set<String> accept;

		answer = "";
		accept = new HashSet<String>(Arrays.asList(acceptArray));
		if (def == null) fullMessage = message + " [y/n] ";
		else fullMessage = message + (def.booleanValue() ? " [Y/n] " : " [y/N] ");

		while (!accept.contains(answer)) {
			System.out.print(fullMessage);

			try {
				answer = br.readLine();
			}
			catch (IOException e) {
				throw new RuntimeException(e);
			}

			if ((def != null) && !accept.contains(answer)) answer = def.booleanValue() ? "y" : "n";
		}

		return answer.equalsIgnoreCase("y");
	}

	public void updateGrid(Grid g) {
		
	}
}