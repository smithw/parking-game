CC=javac
CFLAGS=-Xlint:all -d ./classes -cp ./classes:./libs/simple-xml-2.6.4.jar
ENGINECP=classes/com/politikodrom/parking/engine
ENGINESRC=src/com/politikodrom/parking/engine
ENGINEEXCP=classes/com/politikodrom/parking/engine/exceptions
ENGINEEXSRC=src/com/politikodrom/parking/engine/exceptions
UICP=classes/com/politikodrom/parking/ui/text
UISRC=src/com/politikodrom/parking/ui/text
UTILSCP=classes/com/politikodrom/utils
UTILSSRC=src/com/politikodrom/utils

${UICP}: classes parking.sh ${UICP}/ParkingText.class

${UICP}/ParkingText.class: classes ${UISRC}/ParkingText.java ${UTILSCP} ${ENGINEEXCP} ${ENGINECP}
	${CC} ${CFLAGS} ${UISRC}/ParkingText.java

${ENGINEEXCP}: classes ${ENGINEEXCP}/InconsistentGridException.class ${ENGINEEXCP}/PlayerQuitException.class

${ENGINEEXCP}/InconsistentGridException.class: classes ${ENGINEEXSRC}/InconsistentGridException.java
	${CC} ${CFLAGS} ${ENGINEEXSRC}/InconsistentGridException.java

${ENGINEEXCP}/PlayerQuitException.class: classes ${ENGINEEXSRC}/PlayerQuitException.java
	${CC} ${CFLAGS} ${ENGINEEXSRC}/PlayerQuitException.java

${ENGINECP}: ${ENGINECP}/Movement.class ${ENGINECP}/Block.class ${ENGINECP}/Vehicle.class ${ENGINECP}/Level.class ${ENGINECP}/Grid.class ${ENGINECP}/ParkingUI.class ${ENGINECP}/GameController.class

${ENGINECP}/Grid.class: classes ${ENGINESRC}/Grid.java
	${CC} ${CFLAGS} ${ENGINESRC}/Grid.java

${ENGINECP}/Vehicle.class: classes ${ENGINESRC}/Vehicle.java
	${CC} ${CFLAGS} ${ENGINESRC}/Vehicle.java

${ENGINECP}/GameController.class: classes ${ENGINESRC}/GameController.java
	${CC} ${CFLAGS} ${ENGINESRC}/GameController.java

${ENGINECP}/ParkingUI.class: classes ${ENGINESRC}/ParkingUI.java
	${CC} ${CFLAGS} ${ENGINESRC}/ParkingUI.java

${ENGINECP}/Level.class: classes ${ENGINESRC}/Level.java
	${CC} ${CFLAGS} ${ENGINESRC}/Level.java

${ENGINECP}/Movement.class: classes ${ENGINESRC}/Movement.java
	${CC} ${CFLAGS} ${ENGINESRC}/Movement.java

${ENGINECP}/Block.class: classes ${ENGINESRC}/Block.java
	${CC} ${CFLAGS} ${ENGINESRC}/Block.java

${UTILSCP}: classes ${UTILSCP}/Pair.class

${UTILSCP}/Pair.class: classes ${UTILSSRC}/Pair.java
	${CC} ${CFLAGS} ${UTILSSRC}/Pair.java
	
classes:
	mkdir -p classes

parking.sh:
	cp src/parking.sh ./
	chmod a+x parking.sh

clean:
	rm -rf ./classes
	rm -f parking.sh
