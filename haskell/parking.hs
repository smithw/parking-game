{-# LANGUAGE FlexibleContexts #-}

module Main where

-- This file is just show how ignorantly verbose and unnecessarily convoluted
-- Java is. In a fraction of the time and the code size required by Java, one
-- can implement MORE of the same game in Haskell.

import Control.Monad
import Control.Monad.State
import Control.Applicative
import System.IO
import Data.List
import Data.Maybe
import Data.Char

data Direction = P | N deriving (Show, Eq)
data Orientation = Vertical | Horizontal deriving (Show, Eq)
data Vehicle = Vehicle {
	getLead :: Int,
	getStart :: Int,
	getSize :: Int,
	getOrientation :: Orientation,
	getDescription :: Maybe Char
	} deriving (Show, Eq)

type Level = [Vehicle]
type Block = (Int, Int)
type Grid = [(Block, Maybe Vehicle)]

move :: Direction -> Vehicle -> Vehicle
move d v@(Vehicle { getStart=start }) = v { getStart = start `f` 1 }
	where f = if d == P then (+) else (-)

isPlayer :: Vehicle -> Bool
isPlayer = (==Nothing) . getDescription

findBlocks :: Vehicle -> [Block]
findBlocks (Vehicle {
	getLead=lead,
	getStart=start,
	getSize=size,
	getOrientation=o
	}) = map (mkBlock lead) [start..start+size-1]
	where mkBlock = if o == Vertical then (,) else flip (,)

mkGrid :: Level -> Int -> Grid
mkGrid vs n = mkGridRect vs n n

mkGridRect :: Level -> Int -> Int -> Grid
mkGridRect vs n m = map f blocks
	where
		blocks = flip (,) <$> [0..(n-1)] <*> [0..(m-1)]
		f b = let v = foldl (f' b) Nothing vs in (b, v)
		f' b acc v
			| acc /= Nothing = acc
			| b `elem` findBlocks v = Just v
			| otherwise = Nothing

findVehicles :: Grid -> [Vehicle]
findVehicles = nub . catMaybes . map snd

lookupVehicle :: Char -> Grid -> Maybe Vehicle
lookupVehicle c = listToMaybe . filter f . findVehicles
	where f = (==(Just . toUpper $ c)) . fmap toUpper . getDescription

getPlayer :: Grid -> Maybe Vehicle
getPlayer = listToMaybe . filter isPlayer . findVehicles

findSize:: Grid -> (Int, Int)
findSize = max' . unzip . map fst
	where max' (xs, ys) = (maximum xs + 1, maximum ys + 1)

moveVehicle :: Char -> Direction -> Grid -> Either Grid Grid
moveVehicle c d g = maybe (Left g) id . fmap f . lookupVehicle c $ g
	where f = moveInGrid d g

movePlayer :: Direction -> Grid -> Either Grid Grid
movePlayer d g = maybe (Left g) id . fmap f . getPlayer $ g
	where f = moveInGrid d g

moveInGrid :: Direction -> Grid -> Vehicle -> Either Grid Grid
moveInGrid d g v = case findBlocks v' `intersect` blocks of
	[] -> if start < 0 || start + size > selectBound bounds
		then Left g -- out of bounds
		else Right . uncurry (mkGridRect (v':without)) $ bounds -- success
	_ -> Left g -- collision
	where
		v'@(Vehicle { getStart=start, getSize=size, getOrientation=o }) = move d v
		without = filter (/=v) . findVehicles $ g
		blocks = map fst . filter ((/=Just v) . snd) . filter ((/=Nothing) . snd) $ g
		bounds = findSize g
		selectBound
			| o == Horizontal = fst
			| otherwise = snd


plot :: Grid -> String
plot = ("[ "++) . (++"]") . concat . fst . flip runState 0 . mapM f
	where
		f ((_,y), Nothing) = checkLine y '.'
		f ((_,y), Just v@(Vehicle { getDescription=Nothing })) = checkLine y '+'
		f ((_,y), Just v@(Vehicle { getDescription=(Just c) })) = checkLine y . toUpper $ c
		checkLine y chr = do
			y' <- get
			if y' /= y
				then put y >> (return $ "]\n[ " ++ (chr:" "))
				else return $ chr:" "

pg :: Grid -> IO ()
pg = putStrLn . plot

pe :: Either Grid Grid -> IO ()
pe = either (putStrLn . ("Left:\n"++) . plot) (putStrLn . ("Right:\n"++) . plot) 

level1 :: Level
level1 = [
	Vehicle 2 0 2 Horizontal Nothing,
	Vehicle 5 0 3 Vertical (Just 'a'),
	Vehicle 3 4 2 Horizontal (Just 'b'),
	Vehicle 3 3 2 Vertical (Just 'c')
	]